import * as React from 'react'
import { Link } from 'gatsby'

import '../styles/components/footer.css'

const Footer: React.FC = () => {
  return (
    <footer>
      <svg className="footer-wave" width="1439" height="66" viewBox="0 0 1439 66" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path
          d="M1438.97 28V28.6442C1436.89 38.3488 1330.51 -6.45135 1142 2C996.802 8.50961 888.304 63.2277 727.993 66.8356L694.73 66.8356C706.073 67.0898 717.152 67.0796 727.993 66.8356L1438.97 66.8356V28.6442C1439.01 28.4513 1439.01 28.2368 1438.97 28Z"
          fill="white"
        />
        <path
          d="M5.34387 67.8659C1.57408 69.301 -0.239472 69.4532 0.0253367 67.8659L5.34387 67.8659C29.7938 58.558 136.533 -4.71741 292.244 2.58524C424.503 2.58524 517.51 64.2986 668.414 67.8659L695.73 67.8659C686.41 68.0844 677.311 68.0762 668.414 67.8659L5.34387 67.8659Z"
          fill="white"
        />
      </svg>

      <div className="base-footer">
        <Link className="footer-links left-footer-link" to="/learning">
          learn something.
        </Link>
        <Link className="footer-links right-footer-link" to="/contact">
          talk to us.
        </Link>
      </div>
    </footer>
  )
}
export default Footer
