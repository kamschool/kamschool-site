import * as React from 'react'

import IndexLayout from '../layouts/index'
import Leaf from '../components/leaf'

import '../styles/pages/index.css'

const IndexPage = () => (
  <IndexLayout>
    <div className="blue-bkgnd">
      <svg width="100%" height="100%">
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="80" cy="56" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="240" cy="56" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="430" cy="56" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="610" cy="56" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="800" cy="56" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="990" cy="56" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1180" cy="56" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1370" cy="56" rx="21" ry="21"></ellipse>

        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="5" cy="147" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="155" cy="147" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="335" cy="147" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="525" cy="147" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="705" cy="147" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="895" cy="147" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1085" cy="147" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1275" cy="147" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1435" cy="147" rx="21" ry="21"></ellipse>

        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="80" cy="240" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="240" cy="240" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="430" cy="240" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="610" cy="240" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="800" cy="240" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="990" cy="240" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1180" cy="240" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1370" cy="240" rx="21" ry="21"></ellipse>

        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="5" cy="340" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="155" cy="340" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="335" cy="340" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="525" cy="340" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="705" cy="340" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="895" cy="340" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1085" cy="340" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1275" cy="340" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1435" cy="340" rx="21" ry="21"></ellipse>

        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="80" cy="446" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="240" cy="446" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="430" cy="446" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="610" cy="446" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="800" cy="446" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="990" cy="446" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1180" cy="446" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1370" cy="446" rx="21" ry="21"></ellipse>

        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="5" cy="557" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="155" cy="557" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="335" cy="557" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="525" cy="557" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="705" cy="557" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="895" cy="557" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1085" cy="557" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1275" cy="557" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1435" cy="557" rx="21" ry="21"></ellipse>

        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="80" cy="663" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="240" cy="663" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="430" cy="663" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="610" cy="663" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="800" cy="663" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="990" cy="663" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1180" cy="663" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1370" cy="663" rx="21" ry="21"></ellipse>

        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="5" cy="765" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="155" cy="765" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="335" cy="765" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="525" cy="765" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="705" cy="765" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="895" cy="765" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1085" cy="765" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1275" cy="765" rx="21" ry="21"></ellipse>
        <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1435" cy="765" rx="21" ry="21"></ellipse>
      </svg>
    </div>
    <div className="content">
      <div className="left-content">
        <div className="index-title">
          <h1 className="top-title">KAM</h1>
          <h1 className="bottom-title">SCHOOL</h1>
        </div>
      </div>
      <div className="right-content green-bkgnd">
        <div className="main-section">
          <p className="main-text">
            <b className="kamschool">kamschool</b>
            <div className="main-leaf">
              <Leaf></Leaf>
            </div>
            &mdash;&mdash; is a <b>revolutionary technology education company,</b> focusing on <b>making you better</b> for the sake of the
            internet.
          </p>
        </div>
      </div>
    </div>
  </IndexLayout>
)

export default IndexPage
