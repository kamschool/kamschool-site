# KamSchool Site

> The main site written as a TypeScript-based Gatsby project.

## Tech Stack

- TypeScript
- ESLint (with custom ESLint rules)
- Markdown rendering with Remark
- Basic component structure
- Styling with [emotion](https://emotion.sh/)

## Developing

### Requirements

- gatsby-cli
- yarn
- nodejs >= 6.0.0

```bash
# install dependencies
yarn

# ...or, for npm
npm install

# serve with hot reload at localhost:8000
npm start

# build for production
npm run build

# build for production and push to gh-pages branch
npm run deploy
```

## Deploy

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/resir014/gatsby-starter-typescript-plus)

[Based on Mockup](https://www.canva.com/design/DAD5Atl4QvQ/eAanABbXT7bAByJ2KN3HrA/view?website#1:learning-page)
