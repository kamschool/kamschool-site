import * as React from 'react'

interface ButtonProps {
  text: string
}
const Button: React.FC<ButtonProps> = ({ text }) => (
  <div className="button-wrap">
    <a className="center" target="_blank" rel="noopener noreferrer" href="https://square.site/book/36DASVTTKYQF3/kamschool">
      <button className="general-button">{text}</button>
    </a>
  </div>
)
export default Button
