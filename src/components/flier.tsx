import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import Img from 'gatsby-image'

const Flier: React.FC = () => {
  const data = useStaticQuery(graphql`
    query {
      flier: file(relativePath: { eq: "flier.png" }) {
        ...image
      }
    }
  `)

  return <Img fluid={data.flier.childImageSharp.fluid} />
}

export default Flier
