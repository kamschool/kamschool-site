import * as React from 'react'

interface ContentSectionProps {
  mainHeader?: string
  sectionHeader?: string
  isTopContent?: boolean
}
const ContentSection: React.FC<ContentSectionProps> = ({ mainHeader, sectionHeader, isTopContent, children }) => (
  <div className="main-content">
    {mainHeader && <h1>{mainHeader} </h1>}
    {!isTopContent && <hr className="sep-2" />}
    <div className="section">
      {sectionHeader && <h2 className="section-header">{sectionHeader}</h2>}
      <div className="section-content">{children}</div>
    </div>
  </div>
)

export default ContentSection
