import * as React from 'react'

interface ServiceSectionProps {
  title: string
  publicURL: string
  addPadding?: boolean
}

const ServiceSection: React.FC<ServiceSectionProps> = ({ title, publicURL, addPadding, children }) => {
  return (
    <div className="services-section">
      {addPadding && <h3 className="help-it">{title}</h3>}
      {!addPadding && <h3>{title}</h3>}
      <div className="service-icon">
        <img src={publicURL} />
      </div>
      {children}
    </div>
  )
}

export default ServiceSection
