import * as React from 'react'

const SectionWave: React.FC = () => (
  <svg viewBox="0 0 653 64" version="1.1">
    <g id="Website" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="Home" transform="translate(0.000000, -405.000000)" fill="#C4E8F7">
        <path
          d="M-1,468.908211 C83.9976837,452.470701 121.038965,405.908201 210.400673,405.908201 C299.762381,405.908201 364.814572,468.556639 475.749012,468.556639 C586.683452,468.556639 652.884805,405.974161 652.884805,415.753906 C652.884805,441.71875 653.143993,425.091795 652.884805,445.716795 C652.715293,459.205807 652.884805,468.908211 652.884805,468.908211"
          id="Path"
        ></path>
      </g>
    </g>
  </svg>
)

export default SectionWave
