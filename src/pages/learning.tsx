import * as React from 'react'

import BareLayout from '../layouts/bare'

import '../styles/pages/learning.css'
import LearningBox from '../components/learningBox'

const WorkshopsPage = () => (
  <BareLayout>
    <div className="large-circle-bkgnd">
      <svg width="100%" height="100%">
        <ellipse style={{ strokeWidth: '4', stroke: '#c3e9f1', fill: '#c3e9f1' }} cx="700" cy="5" rx="280" ry="280"></ellipse>
        <ellipse style={{ strokeWidth: '15', stroke: '#c3e9f1', fill: 'none' }} cx="700" cy="5" rx="300" ry="300"></ellipse>

        <ellipse style={{ strokeWidth: '4', stroke: '#00006b', fill: '#00006b' }} cx="1250" cy="750" rx="400" ry="400"></ellipse>

        <ellipse style={{ strokeWidth: '50', stroke: '#bad190', fill: '#bad190' }} cx="200" cy="800" rx="250" ry="250"></ellipse>
      </svg>
    </div>
    <div className="content workshops-content">
      <div className="learning-text">
        <h1 className="learning-header">LEARN SOMETHING</h1>
        <h4 className="learning-subheader">Use these links to help you learn with us!</h4>
      </div>
      <div className="clickable-boxes">
        <div className="boxes-row">
          <LearningBox text="VIDEOS" color="green" link="https://www.youtube.com/channel/UChdSkH8dqdnhnUcTqZEbPkQ/" />
          <LearningBox text="PLAYLISTS" textSize="small" color="grey" link="https://open.spotify.com/user/7fodye8y9ihbvinh7bke4pmij" />
          <LearningBox text="GITLAB" color="green" link="https://gitlab.com/kamschool" />
        </div>
        <div className="boxes-row">
          <LearningBox text="LESSONS" color="dark-blue" disabled={true} />
          <LearningBox text="RESOURCES" textSize="small" color="light-blue" disabled={true} />
        </div>
      </div>
    </div>
  </BareLayout>
)

export default WorkshopsPage
