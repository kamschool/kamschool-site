import * as React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import BareLayout from '../layouts/bare'
import ContactEntry from '../components/contactEntry'
import LogoImage from '../components/logoImage'

import '../styles/pages/contact.css'

const ContactPage = () => {
  const data = useStaticQuery(graphql`
    query {
      twitterImage: file(relativePath: { eq: "twitter.png" }) {
        ...image
      }
      instagramImage: file(relativePath: { eq: "instagram.png" }) {
        ...image
      }
      emailImage: file(relativePath: { eq: "email.png" }) {
        ...image
      }
    }
  `)
  return (
    <BareLayout>
      <div className="blue-bkgnd">
        <svg width="100%" height="100%">
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="80" cy="56" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="240" cy="56" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="430" cy="56" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="610" cy="56" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="800" cy="56" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="990" cy="56" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1180" cy="56" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1370" cy="56" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1560" cy="56" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1750" cy="56" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1940" cy="56" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="2130" cy="56" rx="21" ry="21"></ellipse>

          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="5" cy="147" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="155" cy="147" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="335" cy="147" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="525" cy="147" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="705" cy="147" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="895" cy="147" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1085" cy="147" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1275" cy="147" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1435" cy="147" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1625" cy="147" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1815" cy="147" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="2005" cy="147" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="2195" cy="147" rx="21" ry="21"></ellipse>

          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="80" cy="240" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="240" cy="240" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="430" cy="240" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="610" cy="240" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="800" cy="240" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="990" cy="240" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1180" cy="240" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1370" cy="240" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1560" cy="240" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1750" cy="240" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1940" cy="240" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="2130" cy="240" rx="21" ry="21"></ellipse>

          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="5" cy="340" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="155" cy="340" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="335" cy="340" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="525" cy="340" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="705" cy="340" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="895" cy="340" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1085" cy="340" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1275" cy="340" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1435" cy="340" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1625" cy="340" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1815" cy="340" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="2005" cy="340" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="2195" cy="340" rx="21" ry="21"></ellipse>

          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="80" cy="446" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="240" cy="446" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="430" cy="446" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="610" cy="446" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="800" cy="446" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="990" cy="446" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1180" cy="446" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1370" cy="446" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1560" cy="446" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1750" cy="446" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1940" cy="446" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="2130" cy="446" rx="21" ry="21"></ellipse>

          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="5" cy="557" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="155" cy="557" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="335" cy="557" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="525" cy="557" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="705" cy="557" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="895" cy="557" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1085" cy="557" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1275" cy="557" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1435" cy="557" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1625" cy="557" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1815" cy="557" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="2005" cy="557" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="2195" cy="557" rx="21" ry="21"></ellipse>

          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="80" cy="663" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="240" cy="663" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="430" cy="663" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="610" cy="663" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="800" cy="663" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="990" cy="663" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1180" cy="663" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1370" cy="663" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1560" cy="663" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1750" cy="663" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1940" cy="663" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="2130" cy="663" rx="21" ry="21"></ellipse>

          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="5" cy="765" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="155" cy="765" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="335" cy="765" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="525" cy="765" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="705" cy="765" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="895" cy="765" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1085" cy="765" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1275" cy="765" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1435" cy="765" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1625" cy="765" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1815" cy="765" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="2005" cy="765" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="2195" cy="765" rx="21" ry="21"></ellipse>

          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="80" cy="865" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="240" cy="865" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="430" cy="865" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="610" cy="865" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="800" cy="865" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="990" cy="865" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1180" cy="865" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1370" cy="865" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1560" cy="865" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1750" cy="865" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1940" cy="865" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="2130" cy="865" rx="21" ry="21"></ellipse>

          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="5" cy="965" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="155" cy="965" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="335" cy="965" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="525" cy="965" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="705" cy="965" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="895" cy="965" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1085" cy="965" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1275" cy="965" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1435" cy="965" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1625" cy="965" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="1815" cy="965" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="2005" cy="965" rx="21" ry="21"></ellipse>
          <ellipse style={{ strokeWidth: '4', stroke: '#E9F7FA', fill: '#c3e9f1' }} cx="2195" cy="965" rx="21" ry="21"></ellipse>
        </svg>
      </div>
      <div className="content contact-content">
        <div className="left-content">
          <div className="contact-image">
            <LogoImage />
          </div>
          <div className="left-text">
            <p className="dark-blue contact-us">contact us.</p>
          </div>
          <div className="contact-info">
            <div className="social-accounts accounts">
              <p className="contact-text socials-text">find us on social media.</p>
              <div className="contact-entry">
                <ContactEntry
                  contactImage={data.instagramImage.publicURL}
                  contactLink={'https://instagram.com/kamschool_io'}
                  contactText={'@kamschool_io'}
                />
                <ContactEntry
                  contactImage={data.twitterImage.publicURL}
                  contactLink={'https://twitter.com/kamschool_io'}
                  contactText={'@kamschool_io'}
                />
                <ContactEntry
                  contactImage={data.emailImage.publicURL}
                  contactLink={'mailto:contact@kamschool.io'}
                  contactText={'contact@kamschool.io'}
                />
              </div>
            </div>
          </div>
        </div>
        <svg className="mid-wave" width="106" height="896" viewBox="0 0 106 896" fill="none" xmlns="http://www.w3.org/2000/svg">
          <path
            d="M86.4939 0.999991C86.6639 0.983431 86.8326 0.983372 87 0.999991H86.4939C62.5911 3.32938 14.2824 332.144 54 493C94 655 118.455 845.944 99 896.001H0V0.999991H86.4939Z"
            fill="white"
          />
        </svg>

        <div className="right-content">
          <div className="contact-info">
            <div className="social-accounts accounts">
              <p className="contact-text socials-text">find us on social media.</p>
              <div className="contact-entry">
                <ContactEntry
                  contactImage={data.instagramImage.publicURL}
                  contactLink={'https://instagram.com/kamschool_io'}
                  contactText={'@kamschool_io'}
                />
                <ContactEntry
                  contactImage={data.twitterImage.publicURL}
                  contactLink={'https://twitter.com/kamschool_io'}
                  contactText={'@kamschool_io'}
                />
              </div>
            </div>
            <div className="email-accounts accounts">
              <div className="email-entry">
                <p className="contact-text">send us an email:</p>
                <div className="contact-info">
                  <a className="dark-blue contact-emailaddr" href="mailto:contact@kamschool.io">
                    contact@kamschool.io
                  </a>
                  <ContactEntry
                    contactImage={data.emailImage.publicURL}
                    contactLink={'mailto:contact@kamschool.io'}
                    contactText={'contact@kamschool.io'}
                  />
                </div>
              </div>
              <div className="email-entry">
                <p className="contact-text partner-text">partnership opportunities:</p>
                <div className="contact-info">
                  <a className="dark-blue contact-emailaddr" href="mailto:contact@kamschool.io">
                    kam@kamschool.io
                  </a>
                  <ContactEntry
                    contactImage={data.emailImage.publicURL}
                    contactLink={'mailto:kam@kamschool.io'}
                    contactText={'kam@kamschool.io'}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </BareLayout>
  )
}
export default ContactPage
