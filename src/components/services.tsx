import * as React from 'react'
import { useStaticQuery, graphql } from 'gatsby'

import ServiceSection from './serviceSection'
import Button from './button'

import '../styles/components/services.css'

const Services: React.FC = () => {
  const data = useStaticQuery(graphql`
    query {
      tutoringImage: file(relativePath: { eq: "tutor.svg" }) {
        ...image
      }
      workshopImage: file(relativePath: { eq: "chip.svg" }) {
        ...image
      }
      teachingImage: file(relativePath: { eq: "engineer.svg" }) {
        ...image
      }
    }
  `)

  return (
    <div className="services-group">
      <ServiceSection title="Tutoring" publicURL={data.tutoringImage.publicURL}>
        <p>
          Our <span>one-on-one tutoring</span> empowers students to succeed and expands on course content in detail.
        </p>
      </ServiceSection>
      <ServiceSection title="Workshops" addPadding={true} publicURL={data.workshopImage.publicURL}>
        <p>
          Our <span>workshops</span> connect art and technology to expose K-12 students to programming.
        </p>
      </ServiceSection>
      <ServiceSection title="Teaching" publicURL={data.teachingImage.publicURL}>
        <p>
          Our <span>New Engineer Pathway</span> teaches anyone interested in learning to code.
        </p>
      </ServiceSection>
    </div>
  )
}

export default Services
