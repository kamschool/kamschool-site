import * as React from 'react'

interface ContactEntryProps {
  contactImage: string
  contactText: string
  contactLink: string
}

const ContactEntry: React.FC<ContactEntryProps> = ({ contactImage, contactText, contactLink }) => {
  return (
    <div className="contact-entry-img-box">
      <div className="contact-social-image">
        <a href={contactLink}>
          <img className="contact-entry-img" src={contactImage} />
        </a>
      </div>
    </div>
  )
}

export default ContactEntry
