import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import Img from 'gatsby-image'

const Leaf: React.FC = () => {
  const data = useStaticQuery(graphql`
    query {
      leaf: file(relativePath: { eq: "leaf.png" }) {
        ...image
      }
    }
  `)

  return <Img fluid={data.leaf.childImageSharp.fluid} />
}

export default Leaf
