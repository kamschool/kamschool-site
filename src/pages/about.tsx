import * as React from 'react'

import BareLayout from '../layouts/bare'

import '../styles/pages/about.css'
import video from '../content/kamschool-video.mp4'

const AboutPage = () => (
  <BareLayout>
    <div className="large-circle-bkgnd">
      <svg width="100%" height="100%">
        <ellipse style={{ strokeWidth: '4', stroke: '#c3e9f1', fill: '#c3e9f1' }} cx="90" cy="5" rx="300" ry="300"></ellipse>
        <ellipse style={{ strokeWidth: '15', stroke: '#c3e9f1', fill: 'none' }} cx="90" cy="5" rx="320" ry="320"></ellipse>

        <ellipse style={{ strokeWidth: '4', stroke: '#00006b', fill: '#00006b' }} cx="1200" cy="15" rx="450" ry="450"></ellipse>

        <ellipse style={{ strokeWidth: '50', stroke: '#bad190', fill: 'none' }} cx="80" cy="700" rx="300" ry="300"></ellipse>

        <ellipse style={{ strokeWidth: '4', stroke: '#C3E9F1', fill: '#c3e9f1' }} cx="1200" cy="850" rx="300" ry="300"></ellipse>
      </svg>
    </div>
    <div className="content about">
      <div className="left-content">
        <video className="ks-video" width="650" height="400" autoPlay>
          <source src={video} type="video/mp4" />
        </video>
      </div>
      <div className="right-content">
        <div className="about-content">
          <div className="right-info">
            <h1 className="about-main">WHO ARE WE?</h1>
            <p className="lg-page-text about-text">
              We are a team comprised of real life programmers, troubleshooters, web designers, and marketing professionals, ready to help
              your student take the next step to get a head start of college learning.
            </p>
            <p className="lg-page-text about-text">
              We determined that students between K-12 are more likely to enter career fields that involve tech, based on the amount of
              students that graduated high school in 2013 that became programmers and developers after they graduated college.
            </p>
            <p className="lg-page-text about-text">
              The world today is an ongoing tech industry, and we're here to give students a leg up.
            </p>
          </div>
        </div>
      </div>
    </div>
  </BareLayout>
)

export default AboutPage
