import * as React from 'react'
import Helmet from 'react-helmet'
import { StaticQuery, graphql } from 'gatsby'

import 'modern-normalize'
import '../styles/layout.css'

import Header from '../components/header'
import LayoutRoot from '../components/layoutRoot'
import LayoutMain from '../components/layoutMain'
import Footer from '../components/footer'

import appleIcon57x57 from '../images/favicons/apple-icon-57x57.png'
import appleIcon60x60 from '../images/favicons/apple-icon-60x60.png'
import appleIcon72x72 from '../images/favicons/apple-icon-72x72.png'
import appleIcon76x76 from '../images/favicons/apple-icon-76x76.png'
import appleIcon114x114 from '../images/favicons/apple-icon-114x114.png'
import appleIcon120x120 from '../images/favicons/apple-icon-120x120.png'
import appleIcon144x144 from '../images/favicons/apple-icon-144x144.png'
import appleIcon152x152 from '../images/favicons/apple-icon-152x152.png'
import appleIcon180x180 from '../images/favicons/apple-icon-180x180.png'
import androidIcon192x192 from '../images/favicons/android-icon-192x192.png'
import favicon16 from '../images/favicons/favicon-16x16.png'
import favicon32 from '../images/favicons/favicon-32x32.png'
import favicon96 from '../images/favicons/favicon-96x96.png'

interface StaticQueryProps {
  site: {
    siteMetadata: {
      title: string
      description: string
      keywords: string
    }
  }
}

const IndexLayout: React.FC = ({ children }) => (
  <StaticQuery
    query={graphql`
      query IndexLayoutQuery {
        site {
          siteMetadata {
            title
            description
          }
        }
      }
    `}
    render={(data: StaticQueryProps) => (
      <LayoutRoot>
        <Helmet
          title={data.site.siteMetadata.title}
          meta={[
            { name: 'description', content: data.site.siteMetadata.description },
            { name: 'keywords', content: data.site.siteMetadata.keywords }
          ]}
          link={[
            { rel: 'icon', type: 'image/png', sizes: '16x16', href: `${favicon16}` },
            { rel: 'icon', type: 'image/png', sizes: '32x32', href: `${favicon32}` },
            { rel: 'icon', type: 'image/png', sizes: '96x96', href: `${favicon96}` }
          ]}
        />
        <Header />
        <LayoutMain>{children}</LayoutMain>
        <Footer />
      </LayoutRoot>
    )}
  />
)

export default IndexLayout
