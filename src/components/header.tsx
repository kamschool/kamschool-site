import * as React from 'react'
import { Link } from 'gatsby'

import '../styles/components/header.css'

const Header: React.FC = () => (
  <header>
    <div className="link-bar">
      <Link className="link link-left" to="/about">
        who are we?
      </Link>
      <Link className="link link-right" to="/events">
        interact with us.
      </Link>
      <Link className="link small-link" to="/learning">
        learn something.
      </Link>
      <Link className="link small-link" to="/contact">
        talk to us.
      </Link>
    </div>
    <svg className="header-wave" width="1439" height="108" viewBox="0 0 1439 108" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path
        d="M1436.19 1.32532C1438.26 0.265779 1439.2 0.182933 1438.98 1.33059L1436.19 1.32532C1418.48 10.394 1317.73 91.0128 1147.5 84.045C994.202 77.7703 912.079 26.3764 735 30V0L1436.19 1.32532Z"
        fill="white"
      />
      <path
        d="M0.0312663 57.1274L0.0312663 56.3438C2.08417 45.1755 105.17 116.755 291.511 106.402C444.809 97.8849 558.932 25.0814 736.011 30V0L0.0312663 2L0.0312663 56.3438C-0.0103973 56.5705 -0.0104469 56.8312 0.0312663 57.1274Z"
        fill="white"
      />
    </svg>
  </header>
)

export default Header
