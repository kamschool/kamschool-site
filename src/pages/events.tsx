import * as React from 'react'

import { loadStripe } from '@stripe/stripe-js'
// Make sure to call `loadStripe` outside of a component’s render to avoid
// recreating the `Stripe` object on every render.

import BareLayout from '../layouts/bare'
import Flier from '../components/flier'

import '../styles/pages/events.css'

const stripePromise = loadStripe('pk_test_ozCHHfwl5qEZdDjOpgysm2hx00YxPVBrTG')

const handleClick = async event => {
  event.preventDefault()

  console.log('This is the click button and this should show p')
  // When the customer clicks on the button, redirect them to Checkout.
  const stripe = await stripePromise
  const { error } = await stripe.redirectToCheckout({
    items: [{ plan: 'plan_H9UArxKESoomcP', quantity: 1 }],
    successUrl: 'https://kamschool.io/',
    cancelUrl: 'https://kamschool.io/events'
  })
  // If `redirectToCheckout` fails due to a browser or network
  // error, display the localized error message to your customer
  // using `error.message`.
  if (error) {
    console.log('Error:', error.message)
  }
}
const EventsPage = () => {
  return (
    <BareLayout>
      <div className="content events-content">
        <div className="side-wave-bkgrnd">
          <svg width="1440" height="900" viewBox="0 0 1440 900" fill="none" xmlns="http://www.w3.org/2000/svg">
            <rect width="1440" height="900" fill="white" />
            <path
              d="M69.3952 0H15V900H95C147.114 798.791 143.14 654.363 95 562.5C58.5788 493 68.9387 569 68.9387 484C68.9387 399 153.864 288.152 149.349 202C144.834 115.848 101.796 53.9802 69.3952 0Z"
              fill="#679186"
            />
            <path
              d="M68.5242 0H0V900H68.5242C118.541 798.791 111.824 671.338 91.2461 569C68.5242 456 68.5242 462.5 75.8114 407C83.0986 351.5 133.712 260.152 129.379 174C125.046 87.8479 99.6208 53.9802 68.5242 0Z"
              fill="#B7CB70"
            />
            <path
              d="M1365.26 0H1422.76V900H1326.76C1271.67 798.791 1291.66 695.5 1348.26 606C1389.05 541.5 1358.2 458.969 1348.26 428.5C1338.32 398.031 1326.76 376 1326.76 288C1326.76 200 1331.01 53.9802 1365.26 0Z"
              fill="#264E70"
            />
            <path
              d="M1364.28 0H1439.76V900H1364.28C1309.2 798.791 1282.66 658.5 1339.26 569C1399.24 474.157 1366.2 427.969 1356.26 397.5C1346.32 367.031 1316.73 260.152 1321.5 174C1326.27 87.8479 1330.03 53.9802 1364.28 0Z"
              fill="#C5E9F7"
            />
          </svg>
        </div>
        <div className="left-content">
          <div className="left-container">
            <h1 className="main-header events-header">INTERACT WITH US</h1>
            <div className="inside-box">
              <div className="events-info">
                <div className="title-box">
                  <h2 className="kamcamp-title">TECH EXPEDITION  SUMMER PROGRAM 2K20</h2>
                </div>
                <p className="kamcamp-main-info">
                  <b>Tech Expedition </b> is a remote spin on a traditional summer camp, teaching your students the fundamentals of
                  programming, GitHub, and API.
                </p>
                <div className="events-main">
                  <p className="kamcamp-info">This program includes: </p>
                  <ul className="kamcamp-info">
                    <li>
                      <b>1-on-1 tutoring support</b>
                    </li>
                    <li>
                      <b>Daily lesson plans</b>
                    </li>
                    <li>
                      <b>Meeting professional programmers!</b>
                    </li>
                  </ul>
                  <div className="important-info">
                    <p className="kamcamp-info">
                      <b>Important info:</b>
                    </p>
                    <p className="info-group kamcamp-subinfo" style={{ color: 'red' }}>
                      <b>Registration ends May 31st</b>
                    </p>
                    <p className="kamcamp-subinfo">
                      <b>Cost: $100/week x 4 weeks</b>
                    </p>
                    <p className="kamcamp-subinfo">Scholarships available!</p>
                    <button className="sign-up-button" onClick={handleClick}>
                      SIGN UP!
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="right-content">
          <div className="flier-container">
            <Flier />
          </div>
          <button className="sign-up-button" onClick={handleClick}>
            SIGN UP!
          </button>
        </div>
      </div>
    </BareLayout>
  )
}

export default EventsPage
