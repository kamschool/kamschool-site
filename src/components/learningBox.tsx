import * as React from 'react'

import Leaf from '../components/leaf'

interface LearningBoxProps {
  text: string
  color: string
  disabled?: boolean
  link?: string
  textSize?: string
}

const LearningBox: React.FC<LearningBoxProps> = ({ text, textSize, disabled, link, color }) => {
  textSize = textSize ? textSize : 'large'

  return (
    <a href={link} className="click-width">
      <div className={'box click-box click-box-' + textSize + ' dark-blue ' + color + '-border ' + (disabled ? 'hidden' : '')}>
        <h2 className={'clickbox-text clickbox-text-' + textSize}>{text}</h2>
        <div className="clickbox-leaf">
          <Leaf></Leaf>
        </div>
      </div>
    </a>
  )
}

export default LearningBox
