import * as React from 'react'

import '../styles/components/intro-button.css'

interface ButtonProps {
  title: string
}

const IntroButton: React.FC<ButtonProps> = ({ title }) => <button className="intro-button">{title}</button>

export default IntroButton
