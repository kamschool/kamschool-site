import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import Img from 'gatsby-image'

const LogoImage: React.FC = () => {
  const data = useStaticQuery(graphql`
    query {
      logoImage: file(relativePath: { eq: "main-logo.png" }) {
        ...image
      }
    }
  `)

  return <Img fluid={data.logoImage.childImageSharp.fluid} />
}

export default LogoImage

export const image = graphql`
  fragment image on File {
    childImageSharp {
      fluid(maxWidth: 500) {
        ...GatsbyImageSharpFluid
      }
    }
    extension
    publicURL
  }
`
