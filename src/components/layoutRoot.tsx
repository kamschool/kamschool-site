import * as React from 'react'

interface LayoutRootProps {
  className?: string
}

const LayoutRoot: React.FC<LayoutRootProps> = ({ children, className }) => (
  <>
    <div className={'root ' + className}>{children}</div>
  </>
)

export default LayoutRoot
